public class IntSLList // CLASE ==> Lista Simplemente Enlazada
{
    protected IntSLLNodo head;
    protected IntSLLNodo tail;
    
    public IntSLList(){
       head = tail = null;
    }
    
    public boolean isEmpty(){
       return head == null;
    }
    
    public void addToHead(int el){
       head = new IntSLLNodo(el, head);
       if (tail == null)
          tail = head;
    }
    
    public void addToTail(int el){
       if( !isEmpty() ){
            tail.next = new IntSLLNodo(el);
            tail = tail.next;
        }
        else{
           head = tail = new IntSLLNodo(el);
        }
    }
    
    public int deleteFromHead(){ // Elimina nodo HEAD y da el valor que eliminaste
                                 // es decir el atributo info.
          int el = head.info;
          if( head == tail )  // Pregunta si hay un solo nodo en la LISTA
              head = tail = null;
          else 
              head = head.next; // Nodo HEAD va a ser el segundo Nodo de la LISTA
          return el;
    }
    
    public int deleteFromTail(){ // Elimina ultimo nodo de la lista enlazada
          int el = tail.info;
          if ( head == tail )  // Si es verdad entonces solo hay un nodo en la SLL
            head = tail = null;
          else{ // si hay mas de un nodo hace l siguiente
            IntSLLNodo tmp; // variable para guardar el predecesor del nodo tail
            for( tmp = head; tmp.next != tail ; tmp = tmp.next );
               tail = tmp; //El predecesor del nodo tail ahora es el nodo TAIL
               tail.next = null;
            }
            return el;
    }
    
    public void printAll(){
       for( IntSLLNodo tmp = head; tmp != null; tmp = tmp.next )
            System.out.print( tmp.info + "   " );
    }
    
    public boolean isInList(int el){
       IntSLLNodo tmp;
       for ( tmp = head; tmp != null && tmp.info != el; tmp = tmp.next);
       return tmp != null;
    }
    
    public void delete(int el){ // elimina el primer nodo con el elemento "el"
        if( !isEmpty() )
           if( head == tail && head.info == el ) // si solo hay un nodo en la SLL
              head = tail = null;
           else if( el == head.info ) // si hay mas de un nodo en la SLL
              head = head.next; // se borro el nodo con el elemento y el 2do es el nodo HEAD
                else{
                  IntSLLNodo pred, tmp; // si el elemento no esta en el nodo HEAD
                    for 
                      ( pred = head , tmp = head.next ; 
                        tmp != null && tmp.info != el;
                        pred = pred.next, tmp = tmp.next
                       );
                   if ( tmp != null  ){ // el elemento "el" se encontro
                      pred.next = tmp.next;
                   if( tmp == tail )  // preguntamos si el elemento se hallo en TAIL
                      tail = pred;
            }
            
        }
    }
    
    
}