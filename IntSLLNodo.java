

public class IntSLLNodo  // SLL => Single Linked List
{
   public int info;        // Tipo de dato a almacenar en el nodo
   public IntSLLNodo next; // Direccion de memoria del siguiente nodo
   
   public IntSLLNodo(int i){
        this(i, null);
    }
    
    public IntSLLNodo(int i,IntSLLNodo  n){
        info = i;
        next = n;
    }
}
